module.exports.getAccounts = getAccounts;
var requestJSON = require('request-json');
const URL_BASE = '/apitechu/v3/';
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu19db/collections/';

require('dotenv').config();
const apiKey = process.env.MLAB_API_KEY;

function getAccounts(req, res) {

    let indice = req.params.id;
    var httpClient = requestJSON.createClient(URL_MLAB);
    
    console.log("Cliente HTTP mLab creado");
    var collection = 'accounts?';
    var queryStringID = 'q={"accountID":'+ indice +'}&';
    var queryString = 'f={"_id":0}&';
    httpClient.get(collection + queryStringID + queryString + 'apiKey=' + apiKey,
    function(err, respuestaMLab, body){
    
      console.log(URL_MLAB, 'account?' + queryString + 'apiKey' + apiKey);
      console.log(err, body);
    
      var response = {};
      if(err){
        response = {"msg" : "Error obtenido usuario"}
        res.status(500);
      }else {
        if(body.length > 0) {
          response = body;
        } else {
          response = {"msg" : "Ningun elemento 'user'."}
          res.status(404);
        }
      }
      res.send(response);
    });
  };

  
  