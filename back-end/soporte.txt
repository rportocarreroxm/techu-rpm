npm init
cat package.json
npm install express --save
# Solo para desarrollo
npm install nodemon --save
cat package.json
vim server.js
node server.js

curl -s -H 'Content-Type: application/json' -X GET http://localhost:3000/apitechu/v1/users | jq
curl -s -H 'Content-Type: application/json' -X GET http://localhost:3000/apitechu/v1/users/1 | jq
curl -s -H 'Content-Type: application/json' -X POST http://localhost:3000/apitechu/v1/users -d '{"first_name" : "Renzo", "last_name" : "Portocarrero", "email" : "rportocarrero@bbva.com", "password" : "pepito"}' | jq
curl -s -H 'Content-Type: application/json' -X PUT http://localhost:3000/apitechu/v1/users -d '{"userID" : "3", "first_name" : "Renzo", "last_name" : "Portocarrero", "email" : "rportocarrero@bbva.com", "password" : "pepito"}' | jq
curl -s -H 'Content-Type: application/json' -X DELETE http://localhost:3000/apitechu/v1/users -d '{"userID" : "20"}' | jq

LOGIN
curl -s -H 'Content-Type: application/json' -X POST http://localhost:3000/apitechu/v1/login -d '{"email" : "rportocarrero@bbva.com", "password" : "password"}'

DELETE

curl -s -H 'Content-Type: application/json' -X DELETE http://localhost:3000/apitechu/v1/logout -d '{"email" : "rportocarrero@bbva.com", "password" : "password"}'

curl -s -H 'Content-Type: application/json' -X POST http://localhost:3001/apitechu/v2/logout -d '{"userID" : "1"}'
curl -s -H 'Content-Type: application/json' -X GET 'http://localhost:3001/apitechu/v2/users?inf=2&sup=4' | jq


curl -s -H 'Content-Type: application/json' -X GET http://localhost:3003/apitechu/v3/users | jq
curl -s -H 'Content-Type: application/json' -X GET http://localhost:3003/apitechu/v3/accounts/1 | jq

curl -s -k 'https://api.mlab.com/api/1/databases/techu19db/collections/accounts?q=\{"accountID":5\}&f=\{"_id":0\}&apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF' | jq


curl -s -k 'https://api.mlab.com/api/1/databases/techu19db/collections/accounts?apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF' | jq -r '.[] | {IBAN, userID}'



mocha test/*.js --timeout 15000
