var express = require('express');
var bodyParser = require('body-parser');
// const qr = require('qrcode');
var qr = require('qr-image');
var app = express();
const fs = require('fs');
app.use(bodyParser.json());
var userFile = require('./user.json')
var totalUsers = 0;
var URL_BASE = '/apitechu/v1/';
const PORT = process.env.PORT || 3000;

// GET
//Aqui se pone la URI, y el callback o funcion
app.get(URL_BASE + 'users', function (request, response) {
  //response.send('Helo world');
  console.log('GET ' + URL_BASE + 'users');
  response.send(userFile);
});

// GET por usuario
app.get(URL_BASE + 'users/:id', function (request, response) {
  //console.log('GET ' + URL_BASE + 'users/id');
  //console.log(request.params.id)
  //console.log(request.params)
  let indice = request.params.id;
  let respuesta = userFile[indice - 1];
  //console.log(respuesta)
  let resp = (respuesta != undefined) ? respuesta : {
    "mensaje": "Recurso no encontrado."
  };
  response.status(200);
  response.send(resp);
});

//Peticion GET con Query String
app.get(URL_BASE + 'usersq/', function (request, response) {
  console.log(request.query)
});

// POST
app.post(URL_BASE + 'users', function (request, response) {
  totalUsers = userFile.length;
  cuerpo = request.body;
  if (Object.keys(request.body).length === 0) {
    response.send("No hay datos");
  } else {
    let newUser = {
      "userID": totalUsers + 1,
      "first_name": request.body.first_name,
      "last_name": request.body.last_name,
      "email": request.body.email,
      "password": request.body.password
    }
    userFile.push(newUser);
    response.status(201);
    response.send({
      "mensaje": "usuario creado con exito" + JSON.stringify(newUser)
    });
  }
});

// PUT 
app.put(URL_BASE + 'users', function (req, res) {
  var cntBody = Object.keys(req.body).length;
  if (cntBody > 0) {
    let userFound = userFile[req.body.userID - 1];
    userFound.first_name = req.body.first_name;
    userFound.last_name = req.body.last_name;
    userFound.email = req.body.email;
    userFound.password = req.body.password;

    console.log('Usuario a actualizar ' + JSON.stringify(userFound));

    userFile.splice(req.body.userID - 1, 1, userFound);

    res.status(201);
    res.send({
      "mensaje": "usuario reemplazado " + JSON.stringify(userFile[req.body.userID - 1])
    });


  } else {

    res.send({
      "mensaje": "No se tiene body"
    });

  }

});

// DELETE   
app.delete(URL_BASE + 'users', function (request, response) {
  userFile.splice(request.body.userID - 1, 1);
  response.status(200);
  response.send(userFile);
});

// LOGIN
app.post(URL_BASE + 'login', function(req, res){

  let userFound = false;

  for (let user of userFile) {
      console.log(user);
      if(user.email == req.body.email && req.body.password == user.password){
          userFound = true;
          user.logged = "true";
          writeUserDataToFile(userFile);
          break;
      }
  }


  if(userFound){
      console.log("Login OK");
      res.send({mensaje : "Login exitoso", "user" : req.body.email});
      
  } else {
      console.log("Login failed");
      res.send({mensaje : "usuario o password incorrecto"});
  }

});

// LOGOUT

app.delete(URL_BASE + 'logout', function(req, res){
  let userFound = false;

  for (let user of userFile) {
      if(user.email == req.body.email && user.logged){
          userFound = true;
          delete user.logged;
          writeUserDataToFile(userFile);
          break;
      }
  }


  if(userFound){
      console.log("Logout OK");
      res.send({mensaje : "Logout exitoso", "user" : req.body.email});
      
  } else {
      console.log("Logout failed");
      res.send({mensaje : "usuario no logeado"});
  }

}); 



// FUNCIONES ADICIONALES

function writeUserDataToFile(data) {

  var jsonUserData = JSON.stringify(data);
  console.log(jsonUserData);

  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'user.json'.");
       console.log(data);
     }
   })
}


function reqEmpty(req) {

  return Object.keys(req.body).length !== 0 ? true : false;

}

// QR CODE
//app.get(URL_BASE + 'qr/:text', function(req,res){
    //var qrcode = qr.image(req.params.text, { type: 'png', ec_level: 'H', size: 10, margin: 0 });

app.post(URL_BASE + 'qr', function(req,res){
  var qrcode = qr.image(req.body.qrcode, { type: 'png', size: 15, margin: 1 });
  res.setHeader('Content-type', 'image/png');
  qrcode.pipe(res);
});


app.listen(3000, function () {
  console.log('API escuchando en el puerto 3000...');
});