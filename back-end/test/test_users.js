let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url= 'http://localhost:3003/apitechu/v3';

describe.skip('get all users: ',()=>{

	it('should get all users', (done) => {
		chai.request(url)
			.get('/users')
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});

});

describe.skip('get one Account with userID 1: ',()=>{

	it('should get the Account with userID 1', (done) => {
		chai.request(url)
			.get('/accounts/1')
			.end( function(err,res){
				console.log(res.body)
				expect(res.body[0]).to.have.property('userID').to.be.equal(1);
				expect(res).to.have.status(200);
				done();
			});
	});

});

describe('get users: ',()=>{

	it('should get user with property first_name & last_name ', (done) => {
		chai.request(url)
			.get('/users')
			.end( function(err,res){
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('Array');
                for (user of res.body){
                  expect(user).to.have.property('first_name');
                  expect(user).to.have.property('last_name');
                }
                done();
			});
	});

});
