var express = require('express');
var bodyParser = require('body-parser');
var app = express();

require('dotenv').config();
const apiKey = process.env.MLAB_API_KEY;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var requestJSON = require('request-json');
const URL_BASE = '/apitechu/v3/';
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu19db/collections/';
const PORT = process.env.PORT || 3003;

const account_controller = require('./controllers/account_controller');
const user_controller = require('./controllers/user_controller');
const logout_controller = require('./controllers/logout_controller');
const cors = require('cors');


app.use(cors());
app.options('*', cors());
// GET
//Aqui se pone la URI Externa a MLAB, y el callback o funcion
app.get(URL_BASE + 'users', function (req, res) {
  var httpClient = requestJSON.createClient(URL_MLAB);
  
  console.log("Cliente HTTP mLab creado");

  var queryString = 'f={"_id":0}&';
  httpClient.get('user?' + queryString + 'apiKey=' + apiKey,
  function(err, respuestaMLab, body){
  
    console.log(URL_MLAB, 'user?' + queryString + 'apiKey' + apiKey);
    console.log(err, body);
  
    var response = {};
    if(err){
      response = {"msg" : "Error obtenido usuario"}
      res.status(500);
    }else {
      if(body.length > 0) {
        response = body;
      } else {
        response = {"msg" : "Ningun elemento 'user'."}
        res.status(404);
      }
    }
    res.send(response);
  });
});

// GET Users by id
//curl -s -H 'Content-Type: application/json' -X GET http://localhost:3003/apitechu/v3/users/1
app.get(URL_BASE + 'users/:id', user_controller.getUsers);

// GET Accounts by id
//curl -s -H 'Content-Type: application/json' -X GET http://localhost:3003/apitechu/v3/accounts/1
app.get(URL_BASE + 'accounts/:id', account_controller.getAccounts);

// GET ACCOUNTS
//Aqui se pone la URI Externa de MLAB, y el callback o funcion
// app.get(URL_BASE + 'account/:id', function (req, res) {
//   let indice = req.params.id;
//   var httpClient = requestJSON.createClient(URL_MLAB);
  
//   console.log("Cliente HTTP mLab creado");
//   var collection = 'accounts?';
//   var queryStringID = 'q={"accountID":'+ indice +'}&';
//   var queryString = 'f={"_id":0}&';
//   httpClient.get(collection + queryStringID + queryString + 'apiKey=' + apiKey,
//   function(err, respuestaMLab, body){
  
//     console.log(URL_MLAB, 'account?' + queryString + 'apiKey' + apiKey);
//     console.log(err, body);
  
//     var response = {};
//     if(err){
//       response = {"msg" : "Error obtenido usuario"}
//       res.status(500);
//     }else {
//       if(body.length > 0) {
//         response = body;
//       } else {
//         response = {"msg" : "Ningun elemento 'user'."}
//         res.status(404);
//       }
//     }
//     res.send(response);
//   });
// });

//POST Create new user
// curl -s -H 'Content-Type: application/json' -X POST http://localhost:3003/apitechu/v3/users -d '{"first_name" : "Renzo", "last_name" : "Portocarrero", "email" : "rportocarrero@bbva.com", "password" : "pepito"}'
app.post(URL_BASE + 'users', function (req, res) {
  var httpClient = requestJSON.createClient(URL_MLAB);
  console.log("Cliente HTTP mLab creado");
  httpClient.get('user?' + 'apiKey=' + apiKey,
  function(err, respuestaMLab, body){
  newID =  body.length + 1;
  console.log("newID:" + newID);

  var newUser = {
    "id": newID,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": req.body.password
  };
  httpClient.post('user?' + 'apiKey=' + apiKey, newUser,
  function(err, respuestaMLab, body){
    
    console.log(URL_MLAB, 'user?' + 'apiKey' + apiKey, newUser);
    console.log(err, body);
    res.status(201);
    res.send(body);

  });
});
});

//PUT users con parametro 'id'
// curl -s -H 'Content-Type: application/json' -X PUT http://localhost:3003/apitechu/v3/users/12 -d '{ "first_name" : "Renzosky", "last_name" : "Portocarrero", "email" : "rportocarrero@bbva.com", "password" : "pepito"}'
app.put(URL_BASE + 'users/:id', function(req, res) {
 var id = req.params.id;
 var queryStringID = 'q={"id":' + id + '}&';
 var httpClient = requestJSON.createClient(URL_MLAB);
 httpClient.get('user?'+ queryStringID + 'apiKey=' + apiKey,
   function(err, respuestaMLab, body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    console.log(req.body);
    console.log(cambio);
    httpClient.put(URL_MLAB +'user?' + queryStringID + 'apiKey=' + apiKey, JSON.parse(cambio),
     function(err, respuestaMLab, body) {
       console.log("body:"+ JSON.stringify(body)); // body.n devuelve 1 si pudo hacer el update
      //res.status(200).send(body);
      res.send(body);
     });
   });
});

//DELETE user with id
// curl -s -H 'Content-Type: application/json' -X DELETE http://localhost:3003/apitechu/v3/users/12
app.delete(URL_BASE + 'users/:id', function(req, res){
   var id = req.params.id;
   var queryStringID = 'q={"id":' + id + '}&';
   console.log(URL_MLAB + 'user?' + queryStringID + 'apiKey=' + apiKey);
   var httpClient = requestJSON.createClient(URL_MLAB);
   httpClient.get('user?' +  queryStringID + 'apiKey=' + apiKey,
     function(err, respuestaMLab, body){
       var respuesta = body[0];
       httpClient.delete(URL_MLAB + "user/" + respuesta._id.$oid +'?'+ 'apiKey=' + apiKey,
         function(err, respuestaMLab,body){
           res.send(body);
       });
     });
 });

// Method POST login
// curl -s -H 'Content-Type: application/json' -X POST http://localhost:3003/apitechu/v3/login -d '{"email" : "rportocarrero@bbva.com", "password" : "pepito"}' | jq

app.post(URL_BASE + 'login', function (req, res){
   let email = req.body.email;
   let pass = req.body.password;
   let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
   let limFilter = 'l=1&';
   let httpClient = requestJSON.createClient(URL_MLAB);
   httpClient.get('user?'+ queryString + limFilter + 'apiKey=' + apiKey,
     function(err, respuestaMLab, body) {
      console.log(URL_MLAB, 'user?' + queryString + limFilter + 'apiKey=' + apiKey); 
      console.log(err, body);
       if(!err) {
         if (body.length == 1) { // Existe un usuario que cumple 'queryString'
           let login = '{"$set":{"logged":true}}';
           httpClient.put('user?q={"id": ' + body[0].id + '}&' + 'apiKey=' + apiKey, JSON.parse(login),
           //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
             function(errPut, resPut, bodyPut) {
               res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id});
               // If bodyPut.n == 1, put de mLab correcto
             });
         }
         else {
           res.status(404).send({"msg":"Usuario no válido."});
         }
       } else {
         res.status(500).send({"msg": "Error en petición a mLab."});
       }
   });
});

//POST LOGOUT
// curl -s -H 'Content-Type: application/json' -X POST http://localhost:3003/apitechu/v3/logout -d '{"email" : "rportocarrero@bbva.com", "password" : "pepito"}' | jq
app.post(URL_BASE + 'logout', logout_controller.postLogout);

app.listen(PORT, function () {
  console.log('API escuchando en el puerto 3003...');
});

