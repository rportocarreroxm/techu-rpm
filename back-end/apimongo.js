const express = require("express");
const MongoClient = require("mongoose");
var app = express();

app.get('/api/myApi', (req, res) => {
    MongoClient.connect('mongodb://localhost:27017/local',
      { useNewUrlParser: true }, (err, db) => {
          console.log(db);
  
        if (err) throw err
        const dbo = db.db('local')
        dbo.collection('user')
          .find({}, { _id: 0 })
          .sort({ _id: -1 })
          .toArray(
            (errFind, result) => {
              if (errFind) throw errFind
              const resultJson = JSON.stringify(result)
              console.log('find:', resultJson)
              res.send(resultJson)
              db.close()
            },
          )
      })
  });

  app.listen(3001,function(){
    console.log('API escuchando en el puerto 3001...');
});