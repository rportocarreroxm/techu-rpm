let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url= 'http://localhost:3003/apitechu/v3';

describe('get all users: ',()=>{

	it('should get all users', (done) => {
		chai.request(url)
			.get('/users')
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});

});
